import os
import socket
import threading
from socket import SHUT_RDWR

# 创建socket对象
# socket.socket() 模块名.类名 如果使用from...import...就可以省去模块名
clientSocket = socket.socket()

# 连接到服务端
strIP = "127.0.0.1"
iPort = 1200
clientSocket.connect((strIP, iPort))
print('-' * 10 + "Client" + '-' * 10)
print("Connect Server Successful.")


def SendMessagesToServer(clientSocket):
    while True:
        strMessage: str = input()

        try:
            clientSocket.send(strMessage.encode("utf-8"))
        except OSError as err:
            print("Send OSError", err)
            break

        if strMessage == "Disconnect":
            print("Disconnection...")
            clientSocket.shutdown(SHUT_RDWR)
            clientSocket.close()
            break

    os._exit(0)


def ReceiveServerMessages(clientSocket):
    while True:
        try:
            strMessage: str = clientSocket.recv(1024).decode("utf-8")

            if strMessage != "Disconnect":
                if strMessage != "":
                    print("Server Messages:", strMessage)
                else:
                    print("Server Messages is None.")
            else:
                print("Server Disconnect.")
                clientSocket.shutdown(SHUT_RDWR)
                clientSocket.close()
                break

        except ConnectionAbortedError:
            print("Disconnection...")
            break
        except OSError as err:
            print("Receive OSError", err)
            break

    os._exit(0)


# 多线程
SendThread = threading.Thread(target=SendMessagesToServer, args=(clientSocket,))
RecvThread = threading.Thread(target=ReceiveServerMessages, args=(clientSocket,))
SendThread.start()
RecvThread.start()

# 阻塞
SendThread.join()
RecvThread.join()
